package com.sumerge.grad.program.calculator;

import com.sumerge.grad.program.operation.AddOperation;
import com.sumerge.grad.program.operation.ArithmeticOperation;
import com.sumerge.grad.program.operation.MultiplyOperation;
import com.sumerge.grad.program.operation.SubtractOperation;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Calculator {

    private ArithmeticOperation operation;

    public Double add(Double n1, Double n2) {
        //throw new NotImplementedException();
        operation = new AddOperation();
        return operation.performOperation(n1, n2);
    }

    public Double subtract(Double n1, Double n2) {
        //throw new NotImplementedException();
        operation = new SubtractOperation();
        return operation.performOperation(n1, n2);
    }

    public Double multiply(Double n1, Double n2) {
        //throw new NotImplementedException();
        operation = new MultiplyOperation();
        return operation.performOperation(n1, n2);
    }

    public Double divide(Double n, Double m) {
        //throw new NotImplementedException();
        operation = (Double n1, Double n2) ->  n1/n2;
        return operation.performOperation(n, m);
    }
}
