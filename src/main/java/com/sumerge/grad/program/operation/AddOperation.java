/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumerge.grad.program.operation;

/**
 *
 * @author a
 */
public class AddOperation implements ArithmeticOperation {

    @Override
    public Double performOperation(Double n1, Double n2) {
        return n1+n2;
    }
    
}
